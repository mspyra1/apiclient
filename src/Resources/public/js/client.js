'use strict';

class Client {
    constructor(host, responseField) {
        this.host = host;
        this.responseField = responseField;
        this.httpRequest = new XMLHttpRequest();
        this.httpRequest.onreadystatechange = (function() { this.onResponse(); }).bind(this);
    }

    onResponse() {
        if (this.httpRequest.readyState === XMLHttpRequest.DONE) {
            this.responseField.innerHTML = '<pre>' + JSON.stringify(JSON.parse(this.httpRequest.responseText), undefined, 4) + '</pre>';
        }
    }

    request(method, queryString, data) {
        this.httpRequest.open(method, this.host + (queryString || ''));
        this.httpRequest.setRequestHeader('Content-Type', (method === 'PATCH') ? 'application/merge-patch+json' : 'application/json');
        this.httpRequest.setRequestHeader('Accept','application/json');
        this.httpRequest.send(JSON.stringify(data || ''));
    }

    add(name, amount) {
        this.request('POST', '', { name: name, amount: parseInt(amount) });
    }

    search(name, min, max) {
        let params = new URLSearchParams();
        if (name.length) {
            params.append('name', name);
        }
        if (min.length) {
            params.append('amount[gte]', parseInt(min));
        }
        if (max.length) {
            params.append('amount[lte]', parseInt(max));
        }
        this.request('GET', '?' + params.toString());
    }

    edit(id, name, amount) {
        let data = {};
        if (name.length) {
            data.name = name;
        }
        if (amount.length) {
            data.amount = parseInt(amount);
        }
        if (id.length) {
            this.request('PATCH', '/' + id, data);
        }
    }

    delete(id) {
        if (id.length) {
            this.request('DELETE', '/' + id);
        }
    }
}