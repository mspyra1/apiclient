<?php

namespace Mspyra\Client\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiClientController extends AbstractController
{
    /**
     * @Route("/client/", name="mspyra_client_index")
     */
    public function index(): Response
    {
        return $this->render('@MspyraClient/index.html.twig');
    }
}