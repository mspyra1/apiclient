
# RestApi Client
Simple REST api client created with symfony. Intended to use with my api server (*mspyra/server*)
## Requirements

- PHP >= 7.1
- Symfony 4.4

## Install via composer
Clone repo then run


     composer require mspyra/client

Import routing in **config/routes/annotations.yaml**


     mspyra_client_controllers: 
	     resource: '@MspyraClientBundle/Controller/' 
	     type: annotation  

## Usage

Start web server and navigate to https://localhost/client/ in your browser.  
Fill the API host (default: *localhost/api/items*)
